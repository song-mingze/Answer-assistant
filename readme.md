# 启动文件

> 主界面.py

# 数据库格式

名|类型|长度|不是null|主键|
-|-|-|-|-|
ID|int| | √| |
Topic|text| | √|√|
A|text| | √| |
B|text| | √| |
C|text| | √| |
D|text| | √| |
RightOrError|text| | √| |
FinishOrNot|tinyint|1|√| |
Right_Answer|tinyint|1|√| |

***目前User.py跟questions.py文件没有使用到***

# 目前的功能

1. 数据库存取问题数据
2. 错题保存以及读取
3. 随机获得问题
4. 重置所有题的状态
5. 其余未完成的功能有空就完善

# 软件截图

![img](https://img2023.cnblogs.com/blog/2290421/202306/2290421-20230615180104641-953408984.png)
![img](https://img2023.cnblogs.com/blog/2290421/202306/2290421-20230615180240299-1821897814.png)

# 工程链接

> https://gitee.com/song-mingze/Answer-assistant/tree/main/